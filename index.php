<?php

ini_set('display_errors', '1');
ini_set('display_startup_errors', '1');
error_reporting(E_ALL);

$a = "1";
$b = "3";

$arr = [
    [1, '3', null, 7.7, false],
    [4, 'v', null, 79, false, 88, -4],
];
$arr[1][count($arr[1]) - 1] = 0;
//var_dump($arr[1][1]);

/*echo "<pre>";
print_r($arr);
echo "</pre>";*/

$menu = [
    "first" => [
        "first_disch" => ["price" => 30, "name" => "borsh"],
        "second_disch" => ["price" => 40, "name" => "meat"],
        "salat" => ["price" => 20, "name" => "green salat"]
    ],
    "second" => [
        "first_disch" => ["price" => 30, "name" => "chicken soup"],
        "second_disch" => ["price" => 40, "name" => "popato"],
        "salat" => ["price" => 30, "name" => "cezar"]
    ],
];

$sum = 0;

/*echo $menu["first"]["first_disch"]["name"];
echo "<br>";
echo $menu["second"]["second_disch"]["name"];
echo "<br>";
echo $menu["first"]["salat"]["name"];
echo "<br>";
echo "Sum: " . (
    $menu["first"]["first_disch"]["price"]
    + $menu["second"]["second_disch"]["price"]
    + $menu["first"]["salat"]["price"]
);*/

$first_names = array_column($menu, 'salat');
//print_r(key(current($menu)));

$num = rand(0, 10);
//echo $num . "<br/>";

$daysOfWeek = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"];

$dayIndex = rand(0, count($daysOfWeek) - 1);
$day = $daysOfWeek[$dayIndex];
//echo $day . "<br>";

/*if ($day === "Monday") {
    echo "Понеділок";
} elseif ($day === "Tuesday") {
    echo "Вівторок";
} elseif ($day === "Wednesday") {
    echo "Середа";
} elseif ($day === "Thursday") {
    echo "Четверг";
} elseif ($day === "Friday") {
    echo "П'ятниця";
} elseif ($day === "Saturday") {
    echo "Субота";
} elseif ($day === "Sunday") {
    echo "Неділя";
}*/

/*switch($day) {
    case "Monday":
        echo "Понеділок";
        break;
    case "Tuesday":
        echo "Вівторок";
        break;
    case "Wednesday":
        echo "Середа";
        break;
    case "Thursday":
        echo "Четверг";
        break;
    case "Friday":
        echo "П'ятниця";
        break;
    case "Saturday":
        //echo "Вихідний";
        //break;
    case "Sunday":
        echo "Вихідний";
        break;
}*/

$ukrDay = match ($day) {
    "Monday" => "Понеділок",
    "Tuesday" => "Вівторок",
    "Wednesday" => "Середа",
    "Thursday" => "Четверг",
    "Friday" => "П'ятниця",
    "Saturday", "Sunday" => "Вихідний",
};

//echo $ukrDay;

$month = rand(1, 12);
//echo $month . "<br>";
$period = match ($month) {
    12, 1, 2 => "winter",
    3, 4, 5 => "spring",
    6, 7, 8 => "summer",
    9, 10, 11 => "autumn"
};
//echo $period;

//$num = 2;
/*$i = 1;
while ($num <= 9) {
    while ($i <= 10) {
        echo $num."x".$i." = ".$num * $i."<br>";
        $i++;
    }
    echo "<hr>";
    $i = 1;
    $num++;
}*/

for ($num = 2; $num <= 9; $num++) {
    for ($i = 1; $i <= 10; $i++) {
        //echo $num."x".$i." = ".$num * $i."<br>";
    }
    //echo "<hr>";
}

$a = array(
    "one" => 1,
    "two" => 2,
    "three" => 3,
    "seventeen" => 17
);

foreach ($a as $k => $v) {
    //echo "\$a[$k] => $v.\n";
}

$arr = [1, 3, 0, -5, 6];
$mult = 1;
foreach ($arr as $num) {
    if ($num == 0) {
        //$mult = 0;
        continue;
    }
    $mult = $mult * $num;
}
//echo $mult;

$num = 1000;
$i = 0;
while ($num >= 50) {
    $num /= 2;
    $i++;
    //echo $num . "<br>";
}
//echo $num . " " .$i;

for ($num = 1000, $i = 0; $num >= 50; $num /= 2, $i++) {
    ;
}
//echo $num . " " .$i;

$i = 0;
$max = 99;
while ($i <= $max) {
    //echo ($i < 10) ? "0" . $i. ", " : $i. ", ";
    $i++;
}

$shoppingCart = ["apple" => 10, "tomato" => "50", "chiken" => "140"];
$list = ["orange", "salt", "tomato", "chokolate"];
$newList = [];
foreach ($list as $product) {
    if ( ! isset($shoppingCart[$product])) {
        $newList[] = $product;
    }
}

$a = "Hello world";
$newLine = '';

for ($i = 0; $i < strlen($a); $i++) {
    if ($a[$i] === " ") {
        $newLine = $newLine . "_";
    } else {
        $newLine = $newLine . $a[$i];
    }
}
//echo $newLine;
//echo ord("A");
//$words = explode(" ", $a);
$line = implode("_", explode(" ", $a));
//echo $line;

$str = "A 'quote' is <b>bold</b>";
//echo $str;

// выводит: A 'quote' is &lt;b&gt;bold&lt;/b&gt;
//echo htmlentities($str);

$words = ["green" => "зелений", "жовтий" => "yellow", "red" => "червоний"];
/*$alphavit = 'A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z';
$alphavitArr = explode(",", $alphavit);
$enWords = [];
$uaWords = [];
foreach($words as $k => $v){
    $firstCharWord = strtoupper($k[0]);
    if (in_array($firstCharWord, $alphavitArr)) {
        $enWords[] = $k;
    } else{
        $uaWords[] = $k;
    }

    $firstCharWord = strtoupper($v[0]);
    if (in_array($firstCharWord, $alphavitArr)) {
        $enWords[] = $v;
    } else{
        $uaWords[] = $v;
    }
}*/

$enWords = [];
$uaWords = [];

foreach ($words as $k => $v) {
    if (ord($k[0]) >= 65 && ord($k[0]) <= 122) {
        $enWords[] = $k;
    } else {
        $uaWords[] = $k;
    }

    if (ord($v[0]) >= 65 && ord($v[0]) <= 122) {
        $enWords[] = $v;
    } else {
        $uaWords[] = $v;
    }
}

var_dump($enWords);
var_dump($uaWords);
